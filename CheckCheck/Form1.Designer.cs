﻿namespace CheckCheck
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ControlRead = new System.Windows.Forms.Button();
            this.ControlPanel = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // ControlRead
            // 
            this.ControlRead.Location = new System.Drawing.Point(197, 12);
            this.ControlRead.Name = "ControlRead";
            this.ControlRead.Size = new System.Drawing.Size(75, 23);
            this.ControlRead.TabIndex = 0;
            this.ControlRead.Text = "read file";
            this.ControlRead.UseVisualStyleBackColor = true;
            this.ControlRead.Click += new System.EventHandler(this.ControlRead_Click);
            // 
            // ControlPanel
            // 
            this.ControlPanel.AllowDrop = true;
            this.ControlPanel.AutoSize = true;
            this.ControlPanel.Location = new System.Drawing.Point(12, 41);
            this.ControlPanel.Name = "ControlPanel";
            this.ControlPanel.Size = new System.Drawing.Size(260, 208);
            this.ControlPanel.TabIndex = 2;
            this.ControlPanel.DragDrop += new System.Windows.Forms.DragEventHandler(this.ControlPanel_DragDrop);
            // 
            // Form1
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 41);
            this.Controls.Add(this.ControlPanel);
            this.Controls.Add(this.ControlRead);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Form1";
            this.Text = "Check as you wish";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.Form1_DragDrop);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ControlRead;
        private System.Windows.Forms.Panel ControlPanel;
    }
}

