﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CheckCheck
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            //ControlPanel.AutoScroll = false;
            //ControlPanel.HorizontalScroll.Enabled = false;
            //ControlPanel.HorizontalScroll.Visible = false;
            //ControlPanel.HorizontalScroll.Maximum = 0;
            //ControlPanel.AutoScroll = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (Environment.GetCommandLineArgs().Length > 1)
                ReadFile(Environment.GetCommandLineArgs()[1]);
        }

        public void ReadFile(string path)
        {
            Checkbox cb = new Checkbox(path, this, ControlPanel);
            cb.ReadFromFile();
        }

        private void ControlRead_Click(object sender, EventArgs e)
        {
            //ReadFile(ControlPath.Text);
            OpenFile();
        }

        private void OpenFile()
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = 
                    Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                openFileDialog.Filter = "All files (*.*)|*.*";
                openFileDialog.FilterIndex = 0;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                    ReadFile(openFileDialog.FileName);
            }
        }

        private void Form1_DragDrop(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.All;
            var test = e.Data;
            Console.WriteLine();
        }

        private void ControlPanel_DragDrop(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.All;
            e.ToString();
        }
    }
}
