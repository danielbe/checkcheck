﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CheckCheck
{
    class Checkbox
    {
        Form form;
        Panel panel;
        string path;

        public Checkbox(string path, Form form, Panel panel)
        {
            this.panel = panel;
            this.form = form;
            this.path = path;
        }

        public void ReadFromFile()
        {
            int counter = 0;
            string line;
            if (File.Exists(@path))
            {
                try
                {
                    StreamReader file = new StreamReader(@path);
                    while ((line = file.ReadLine()) != null)
                    {
                        line = line.Trim();
                        if (!string.IsNullOrWhiteSpace(line))
                        {
                            CreateCheckbox(counter, line);
                            counter++;
                        }
                    }

                    file.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, $"Error with path {path}");
                }
            }
            else
                MessageBox.Show($"\"{path}\" does not seem to be valid");
        }

        private void CreateCheckbox(int counter, string text)
        {
            CheckBox check = new CheckBox();
            check.Location = new Point(0, 20 * (counter));
            check.Name = "check" + counter;
            check.Text = text;
            check.AutoSize = true;
            panel.Controls.Add(check);
            form.Size = new Size(300, 100 + counter * 20);
            panel.Size = new Size(panel.Size.Width, panel.Size.Height + 18);
        }
    }
}
